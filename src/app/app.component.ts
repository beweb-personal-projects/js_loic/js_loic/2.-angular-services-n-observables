import { Component } from '@angular/core';
import { DataMessagesService } from './services/data-messages/data-messages.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'app';

  constructor(
    private messagesData: DataMessagesService
  ) {}

  public getMessagesUnread(): number {
    let tempnumber: number = 0;
    this.messagesData.getMessagesUnread().subscribe((messages) => tempnumber = messages);
    return tempnumber;
  }
}

