import { Component, OnInit } from '@angular/core';
import { DataMessagesService } from 'src/app/services/data-messages/data-messages.service';
import { Message_Impl } from '../message/message.component';

@Component({
  selector: 'app-createmessage',
  templateUrl: './createmessage.component.html',
  styleUrls: ['./createmessage.component.scss']
})
export class CreatemessageComponent implements OnInit {

  m: Message_Impl = {
    subject: '',
    content: '',
    isRead: false
  }

  constructor(
    private messagesData: DataMessagesService
  ) { }

  ngOnInit() {}

  addMessage(): void {
    this.messagesData.addDataMessages({
      subject: this.m.subject,
      content: this.m.content,
      isRead: this.m.isRead
    });

    this.resetInputs();
  }

  resetInputs(): void {
    this.m.subject = '';
    this.m.content = '';
  }
}
