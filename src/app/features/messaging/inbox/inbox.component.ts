import { Component, OnInit } from '@angular/core';
import { DataMessagesService } from 'src/app/services/data-messages/data-messages.service';
import { Message_Impl } from '../message/message.component';

@Component({
  selector: 'app-inbox',
  templateUrl: './inbox.component.html',
  styleUrls: ['./inbox.component.scss']
})
export class InboxComponent implements OnInit {

  messages: Message_Impl[] = [];

  constructor(
    private messagesData: DataMessagesService
  ) { }

  ngOnInit() {
    this.messagesData.getDataMessages().subscribe((messages) => this.messages = messages);
  }

}
