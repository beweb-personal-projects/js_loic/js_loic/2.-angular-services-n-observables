import { Component, Input, OnInit } from '@angular/core';
import { DataMessagesService } from 'src/app/services/data-messages/data-messages.service';

@Component({
  selector: 'app-message',
  templateUrl: './message.component.html',
  styleUrls: ['./message.component.scss']
})
export class MessageComponent implements OnInit {

  @Input('data') message!: Message_Impl;

  constructor(
    private messagesData: DataMessagesService
  ) { }

  ngOnInit() {}

  deleteMessage(): void {
    this.messagesData.removeDataMessage(this.message);
  }

  readMessage(): void {
    this.messagesData.readDataMessage(this.message);
  }

}

export interface Message_Impl {
  subject: string;
  content: string;
  isRead: boolean;
}
