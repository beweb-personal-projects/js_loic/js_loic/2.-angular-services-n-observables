import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { CreatemessageComponent } from './createmessage/createmessage.component';
import { DeletemessageComponent } from './deletemessage/deletemessage.component';
import { InboxComponent } from './inbox/inbox.component';
import { MessageComponent } from './message/message.component';
import { NotificationComponent } from './notification/notification.component';
import { SendmailComponent } from './sendmail/sendmail.component';



@NgModule({
  declarations: [
    InboxComponent,
    SendmailComponent,
    MessageComponent,
    CreatemessageComponent,
    DeletemessageComponent,
    NotificationComponent
  ],
  imports: [
    CommonModule,
    FormsModule
  ],
  exports: [
    InboxComponent,
    SendmailComponent,
    MessageComponent,
    CreatemessageComponent,
    DeletemessageComponent,
    NotificationComponent
  ]
})
export class MessagingModule { }
