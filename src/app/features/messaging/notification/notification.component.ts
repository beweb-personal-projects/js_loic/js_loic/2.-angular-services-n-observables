import { Component, OnInit } from '@angular/core';
import { map } from 'rxjs';
import { DataMessagesService } from 'src/app/services/data-messages/data-messages.service';

@Component({
  selector: 'app-notification',
  templateUrl: './notification.component.html',
  styleUrls: ['./notification.component.scss']
})
export class NotificationComponent implements OnInit {

  notReadMessages: number = 0;

  constructor(
    private messagesData: DataMessagesService
  ) { }

  ngOnInit() {
    this.messagesData
      .getMessagesUnreadCountSubject()
      .subscribe(unreadMessagesCount => this.notReadMessages = unreadMessagesCount);
  }

}
