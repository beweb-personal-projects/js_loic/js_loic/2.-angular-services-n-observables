import { Injectable } from '@angular/core';
import { BehaviorSubject, map, Observable, of } from 'rxjs';
import { Message_Impl } from 'src/app/features/messaging/message/message.component';

@Injectable({
  providedIn: 'root'
})
export class DataMessagesService {

  private data: Message_Impl[] = [
    {
      subject: 'Hello First',
      content: 'Content First',
      isRead: false
    },
    {
      subject: 'Hello Second',
      content: 'Content Second',
      isRead: true
    },
    {
      subject: 'Hello Third',
      content: 'Content Third',
      isRead: true
    }
  ]

  private subject = new BehaviorSubject<Message_Impl[]>([]);

  constructor() {
    this.subject.next([
      {
        subject: 'Hello First',
        content: 'Content First',
        isRead: false
      },
      {
        subject: 'Hello Second',
        content: 'Content Second',
        isRead: true
      },
      {
        subject: 'Hello Third',
        content: 'Content Third',
        isRead: true
      }
    ]);
  }

  public getDataMessages(): Observable<Message_Impl[]> {
    return of(this.data);
  }

  public getDataMessagesSubject(): Observable<Message_Impl[]> {
    return this.subject.asObservable();
  }

  public addDataMessages(message: Message_Impl): void {
    //Basic push
    this.data.push(message);

    //Observer Push
    const msgs = this.subject.getValue();
    msgs.push(message);
    this.subject.next(msgs);
  }

  public removeDataMessage(message: Message_Impl): void {
    this.data.splice(this.data.indexOf(message), 1);
  }

  public readDataMessage(message: Message_Impl): void {
    this.data[this.data.indexOf(message)].isRead = true;
  }

  public getMessagesUnread(): Observable<number> {
    return of(this.data.filter(message => !message.isRead).length);
  }

  public getMessagesUnreadSubject(): Observable<Message_Impl[]> {
    return this.subject
      .pipe(
        map((msgs) => msgs.filter((m) => !m.isRead))
      )
  }

  public getMessagesUnreadCountSubject(): Observable<number> {
    return this.subject
      .pipe(
        map((msgs) => msgs.filter((m) => !m.isRead).length),
        // map(urm => urm.length)
      )
  }
}
